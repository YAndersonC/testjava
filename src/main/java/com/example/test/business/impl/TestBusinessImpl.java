package com.example.test.business.impl;

import com.example.test.business.TestBusinessInterface;
import com.example.test.business.dao.TestRepositoryCategoryInterface;
import com.example.test.business.dao.TestRepositoryInterface;
import com.example.test.business.model.BlogPost;
import com.example.test.business.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Business Class responsible for implement business logic
 */
@Service
public class TestBusinessImpl implements TestBusinessInterface {

    @Autowired
    private TestRepositoryInterface testRepositoryInterface;
    @Autowired
    private TestRepositoryCategoryInterface testRepositoryCategoryInterface;

    public List<BlogPost> listAll() {
        return testRepositoryInterface.findAll();
    }

    public BlogPost save(BlogPost payload) {
        testRepositoryInterface.save(payload);
        return payload;
    }

    public BlogPost get(Integer id) {
        return testRepositoryInterface.findById(id).get();
    }

    public List<Category> getCategories() {
        return testRepositoryCategoryInterface.findAll();
    }

    public void delete(Integer id) {
        testRepositoryInterface.deleteById(id);
    }

    public void deleteAll() {
        testRepositoryInterface.deleteAll();
    }

    public ResponseEntity<BlogPost> update(BlogPost payload, Integer id){
        try {
            BlogPost existBlogPost = this.get(id);
            existBlogPost.setTitle(payload.getTitle());
            existBlogPost.setContents(payload.getContents());
            existBlogPost.setCategory(payload.getCategoryId());
            existBlogPost.setCategory(payload.getCategory());
            this.save(existBlogPost);
            return new ResponseEntity<>(payload, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(payload,HttpStatus.NOT_FOUND);
        }
    }

}
