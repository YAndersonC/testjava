package com.example.test.business.dao;

import com.example.test.business.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface responsible for database categories transactions
 */
public interface TestRepositoryCategoryInterface extends JpaRepository<Category,Integer> {
}
