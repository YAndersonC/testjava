package com.example.test.business.dao;

import com.example.test.business.model.BlogPost;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface responsible for database transactions
 */
public interface TestRepositoryInterface extends JpaRepository <BlogPost,Integer>{

}
