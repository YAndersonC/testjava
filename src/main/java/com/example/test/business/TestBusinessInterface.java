package com.example.test.business;

import com.example.test.business.model.BlogPost;
import com.example.test.business.model.Category;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TestBusinessInterface {

    public List<BlogPost> listAll();

    public BlogPost save(BlogPost product);

    public BlogPost get(Integer id);

    public List<Category> getCategories();

    public void delete(Integer id);

    public void deleteAll();

    public ResponseEntity<BlogPost> update(BlogPost product, Integer id);
}
