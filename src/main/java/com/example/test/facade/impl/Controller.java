package com.example.test.facade.impl;

import com.example.test.business.TestBusinessInterface;
import com.example.test.business.model.BlogPost;
import com.example.test.business.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Class responsible for expose the API
 */
@RestController
public class Controller {

    @Autowired
    private TestBusinessInterface businessInterface;

    @GetMapping("/posts")
    public List<BlogPost> list() {
        return businessInterface.listAll();
    }

    @GetMapping("/posts/{id}")
    public BlogPost obtain(@PathVariable Integer id) {
        return businessInterface.get(id);
    }

    @GetMapping("/categories")
    public List<Category> listCategories() {
        return businessInterface.getCategories();
    }

    @PostMapping("/posts")
    public BlogPost add(@RequestBody BlogPost payload) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern ( "yyyy/MM/dd HH:mm:ss.SSS" );
        payload.setTimestamp(formatter.format(LocalDateTime.now()));
        businessInterface.save(payload);
        return payload;
    }

    @PutMapping("/posts/{id}")
    public ResponseEntity<?> update(@RequestBody BlogPost payload, @PathVariable Integer id) {
        return businessInterface.update(payload,id);
    }

    @DeleteMapping("/posts")
    public void deleteAll() {
        businessInterface.deleteAll();
    }

    @DeleteMapping("/posts/{id}")
    public void delete(@PathVariable Integer id) {
        businessInterface.delete(id);
    }

}
